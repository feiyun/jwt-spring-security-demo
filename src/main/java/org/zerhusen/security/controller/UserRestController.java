package org.zerhusen.security.controller;

import javax.servlet.http.HttpServletRequest;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.zerhusen.security.JwtTokenUtil;
import org.zerhusen.security.JwtUser;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
public class UserRestController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${permsvc.url}")
    private String permHostUrl;

    @Value("${app.domain}")
    private String appDomain;
    @RequestMapping(value = "user", method = RequestMethod.GET)
    public JwtUser getAuthenticatedUser(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
        return user;
    }

    @RequestMapping(value = "perms", method = RequestMethod.GET)
    public ResponseEntity getPermissions(HttpServletRequest request) {
        String domain = String.valueOf(request.getParameter("domain"));
        String inst = request.getParameter("inst");
        String token = request.getHeader(tokenHeader).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        List<Map<String, String>> res = Lists.newArrayList();
        if (!Strings.isNullOrEmpty(domain)) {

            log.info("domain: {}, inst: {}", domain, inst);
            /*
             * Fetch the roles info from the permission service
             * */
            String requestURL = String.format(permHostUrl + "/domains/%s/users/%s/permissions", domain, username);
            if (inst != null) {
                requestURL = String.format(permHostUrl + "/domains/%s/users/%s/permissions?inst=%s", domain, username, inst);
            }
            log.info("requestURL:{}",requestURL);
            List<Map<String, String>> perms = restTemplate.getForObject(requestURL, List.class);
            if (perms != null) {
                /*for (Map<String, String> perm : perms) {
                    res.putIfAbsent(perm.get("sub"), Lists.newArrayList());
                    res.get(perm.get("sub")).add(perm);
                }*/
                return ResponseEntity.ok(perms);
            }
        }
        return ResponseEntity.ok(res);
    }
}
