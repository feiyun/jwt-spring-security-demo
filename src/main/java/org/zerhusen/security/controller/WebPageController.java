package org.zerhusen.security.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Controller
public class WebPageController {

    @RequestMapping(value = "apps/{domain}", method = RequestMethod.GET)
    public String loginPage(HttpServletRequest request, @PathVariable(name = "domain") String domain,
                           @RequestParam(required = false) String inst) {
        request.setAttribute("domain", domain);
        request.setAttribute("inst", inst);
        log.info("domain: {}, inst: {}", domain, inst);
        return "/page/show.html";
    }
    @RequestMapping("hello")
    public ModelAndView hello(String name, Model model) {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        model.addAttribute("name", name);
        model.addAttribute("strList", list);
        model.addAttribute("show", false);
        ModelAndView mv = new ModelAndView();
        mv.addObject("user", name);
        mv.setViewName("/page/show.html");
        return mv;
    }
}
