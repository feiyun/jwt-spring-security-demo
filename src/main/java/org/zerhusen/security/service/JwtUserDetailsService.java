package org.zerhusen.security.service;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.zerhusen.model.security.Authority;
import org.zerhusen.model.security.AuthorityName;
import org.zerhusen.model.security.User;
import org.zerhusen.security.JwtUserFactory;
import org.zerhusen.security.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Log4j2
@Service("jwtUserDetailsService")
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${permsvc.url}")
    private String permHostUrl;

    @Value("${app.domain}")
    private String appDomain;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            String domain = String.valueOf(request.getParameter("domain"));
            String inst = request.getParameter("inst");

            if (!Strings.isNullOrEmpty(domain)) {

                log.info("domain: {}, inst: {}", domain, inst);
                /*
                 * Fetch the roles info from the permission service
                 * */
                String requestURL = String.format(permHostUrl + "/domains/%s/users/%s/roles", domain, user.getUsername());
                if (inst != null) {
                    requestURL = String.format(permHostUrl + "/domains/%s/users/%s/roles?inst=%s", domain, user.getUsername(), inst);
                }
                List<Map> roles = restTemplate.getForObject(requestURL, List.class);

                List<Authority> authorities = Lists.newArrayList();
                if (roles != null && roles.size() > 0) {
                    String role = "ROLE_" + roles.get(0).get("key");
                    Authority authority = new Authority();
                    authority.setId(0L);
                    AuthorityName authrityName = AuthorityName.valueOf(role.toUpperCase());
                    authority.setName(authrityName);
                    authorities.add(authority);
                }
                user.setAuthorities(authorities);
            }
            return JwtUserFactory.create(user);
        }
    }
}
