package org.zerhusen.aop;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Log4j2
@Aspect
@Component
public class AccessControlAspect {
    @Pointcut(value = "@annotation(org.zerhusen.aop.AccessControl)")
    public void annotationPointCut() {
    }
 
    @Around("annotationPointCut()")
    public Object doAround(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String methodName = signature.getMethod().getName();
        log.info("method name：{}", methodName);
 
        if(!accessControlValidate()){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        try {
            return joinPoint.proceed();
        } catch (Throwable throwable) {
            return null;
        }
    }
    private boolean accessControlValidate(){
        return false;
    }
}
